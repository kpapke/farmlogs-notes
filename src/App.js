import React from 'react';
import NoteContainer from './containers/NoteContainer';
import styles from './App.css';

function App() {
  return (
    <div className={styles.app}>
      <div className={styles.appHeader}>
        <h1>Field Notes</h1>
        <p>
          Created by Kyle Papke using <a href="https://github.com/facebookincubator/create-react-app">create-react-app</a> and <a href="http://react-toolbox.com/">React Toolbox</a>
        </p>
      </div>
      <NoteContainer />
    </div>
  );
}

export default App;
