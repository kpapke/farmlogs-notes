import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import sessionStorage from 'mock-local-storage';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});
