import React, { Component } from 'react';
import { Card, CardMedia, CardTitle, CardText, CardActions } from 'react-toolbox/lib/card';
import { Button } from 'react-toolbox/lib/button';
import PropTypes from 'prop-types';
import styles from './styles.css';

/**
 * Note component styles the content for each note card
 * The note's content and edit/delete actions are passed down from NoteContainer
 */

class Note extends Component {

  render() {
    return (
      <Card className={styles.note} >
        <CardTitle
          avatar={this.props.author.avatarUrl}
          title={this.props.author.name}
          subtitle={this.props.date}
        />
        <CardMedia
          aspectRatio="wide"
          image={this.props.content.photo}
        />    
        <CardTitle
          title={this.props.content.title}
        />
        <CardText>{this.props.content.text}</CardText>
        <CardActions>
          <Button 
            onClick={this.props.editNote} 
            label="Edit" 
          />
          <Button 
            onClick={this.props.deleteNote} 
            label="Delete" 
          />
        </CardActions>
      </Card>
    );
  }
}

Note.PropTypes = {
  id: PropTypes.number.isRequired,
  date: PropTypes.string.isRequired,
  author: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
  editNote: PropTypes.func.isRequired,
  deleteNote: PropTypes.func.isRequired
}

export default Note;