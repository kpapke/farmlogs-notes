import React from 'react';
import { shallow, mount } from 'enzyme';
import sessionStorage from 'mock-local-storage';
import Note from '../../Note';
import { Card, CardMedia, CardTitle, CardText, CardActions } from 'react-toolbox/lib/card';
import { Button } from 'react-toolbox/lib/button';

let enzymeWrapper;

const props = {
    "id": 1,
    "date": "7-10-17",
    "author": {
        "name": "Tony",
        "avatarUrl": "https://placeimg.com/80/80/animals"
    },
    "content": {
        "title": "Title of First Note",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim. Negat enim summo bono afferre incrementum diem. Cupiditates non Epicuri divisione finiebat, sed sua satietate. Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia.",
        "photo": "https://placeimg.com/800/450/tech/sepia"
    }
};

beforeEach(() => {
    enzymeWrapper = shallow(<Note {...props} />);
});

describe('Note component', () => {
    it('should render with child components', () => {
        expect(enzymeWrapper.find(Card)).toHaveLength(1);
        expect(enzymeWrapper.find(CardTitle)).toHaveLength(2);
        expect(enzymeWrapper.find(CardMedia)).toHaveLength(1);
        expect(enzymeWrapper.find(CardText)).toHaveLength(1);
        expect(enzymeWrapper.find(Button)).toHaveLength(2);
    });
    
    it('have correct props', () => {
        const expectedTitle = enzymeWrapper.find(CardTitle).at(2).props();
        expect(expectedTitle.title).toBe(props.title);

        const expectedText = enzymeWrapper.find(CardText).first().props();
        expect(expectedText.text).toBe(props.text);
    });
});