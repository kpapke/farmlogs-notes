import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from 'react-toolbox/lib/dialog';
import NoteForm from '../NoteForm';

/**
 * NoteDialog component opens a dialog containing a form to edit or add a new note
 * The note model to be added or edited (activeNote) is passed from NoteContainer
 * The dialog also has click handlers for canceling or saving the note
 */

class NoteDialog extends Component {

  actions = [
    { 
			label: 'Cancel', 
			onClick: this.props.closeDialog 
		},
    { 
			label: 'Save', 
			onClick: this.props.saveDialog 
		}
  ];

	getDialogTitle = () => {
		return this.props.activeNoteContent.title ? 'Edit Note' : 'Add Note';
	}

  render () {
    return (
      <div>
        <Dialog
          actions={this.actions}
          active={this.props.active}
          onEscKeyDown={this.props.closeDialog}
          onOverlayClick={this.props.closeDialog}
					title={this.getDialogTitle()}
        >
          <NoteForm
						activeNoteContent={this.props.activeNoteContent}
						handleNoteUpdate={this.props.handleNoteUpdate} 
					/>
        </Dialog>
      </div>
    );
  }
}

NoteDialog.PropTypes = {
	active: PropTypes.bool.isRequired,
	closeDialog: PropTypes.func.isRequired,
	saveDialog: PropTypes.func.isRequired,
	activeNoteContent: PropTypes.object.isRequired,
	handleNoteUpdate: PropTypes.func.isRequired
}

export default NoteDialog;