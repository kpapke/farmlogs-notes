import React from 'react';
import { shallow, mount } from 'enzyme';
import sessionStorage from 'mock-local-storage';
import NoteDialog from '../../NoteDialog';
import NoteForm from '../../NoteForm';
import Dialog from 'react-toolbox/lib/dialog';

let enzymeWrapper;

const props = {
    active: false,
    closeDialog: () => {},
    saveDialog: () => {},
    activeNoteContent: {},
    handleNoteUpdate: () => {} 
};

const actions = [
    { 
        label: 'Cancel', 
        onClick: props.closeDialog 
    },
    { 
        label: 'Save', 
        onClick: props.saveDialog 
    }
];

beforeEach(() => {
    enzymeWrapper = shallow(<NoteDialog {...props} />);
});

describe('NoteDialog component', () => {
    it('should render with child components', () => {
        expect(enzymeWrapper.find(Dialog)).toHaveLength(1);
        expect(enzymeWrapper.find(NoteForm)).toHaveLength(1);
    });
    
    it('have correct props', () => {
        const expectedProps = enzymeWrapper.find(Dialog).first().props();
        expect(expectedProps.active).toBe(props.active);
        expect(expectedProps.actions).toEqual(actions);
        expect(expectedProps.onEscKeyDown).toBe(props.closeDialog);
        expect(expectedProps.onOverlayClick).toBe(props.closeDialog);
        expect(expectedProps.title).toBe('Add Note');
    });
});