import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Input from 'react-toolbox/lib/input';
import styles from './styles.css';

/**
 * NoteForm component holds the note content to be added or edited inside NoteDialog
 * The active note and event handlers are passed down from NoteDialog
 * Currently only title and text are editable (Author, date, photo etc are hard coded)
 */

class NoteForm extends Component {
  render () {
    return (
      <section>
        <Input 
          className={styles.inputBox}
          type='text'
          label='Title'
          value={this.props.activeNoteContent.title} 
          required 
          onChange={(value) => this.props.handleNoteUpdate('title', value)} 
        />
        <Input
          className={styles.inputBox}
          type='text'
          label='Description'
          multiline
          value={this.props.activeNoteContent.text} 
          required 
          onChange={(value) => this.props.handleNoteUpdate('text', value)} 
        />
      </section>
    );
  }
}

NoteForm.PropTypes = {
  activeNoteContent: PropTypes.object.isRequired,
  handleNoteUpdate: PropTypes.func.isRequired
}

export default NoteForm;