import React from 'react';
import { shallow, mount } from 'enzyme';
import sessionStorage from 'mock-local-storage';
import NoteForm from '../../NoteForm';
import Input from 'react-toolbox/lib/input';

let enzymeWrapper;

const props = {
    activeNoteContent: {
        title: 'Test Title'
    },
    handleNoteUpdate: () => {} 
};

beforeEach(() => {
    enzymeWrapper = shallow(<NoteForm {...props} />);
});

describe('NoteForm component', () => {
    it('should render with child components', () => {
        expect(enzymeWrapper.find('section')).toHaveLength(1);
        expect(enzymeWrapper.find(Input)).toHaveLength(2);
    });
    
    it('have correct props', () => {
        const expectedProps = enzymeWrapper.find(Input).first().props();
        expect(expectedProps.value).toBe(props.activeNoteContent.title);
    });
});