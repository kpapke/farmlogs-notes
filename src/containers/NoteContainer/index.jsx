import React, { Component } from 'react';
import { Button } from 'react-toolbox/lib/button';
import Note from '../../components/Note';
import NoteDialog from '../../components/NoteDialog';
import initialSessionData from './temp/data.json';
import styles from './styles.css';

/**
 * NoteContainer holds the majority of state for reading/writing to browser sessionStorage
 * It allows the user to view, add, edit, and delete notes
 * The state for notes (activeNote) is passed to NoteDialog
 * The content for notes is passed to Note
 */

class NoteContainer extends Component {

  // get data from session or get initial data on page load
  
  getSessionData = () => {
    return JSON.parse(window.sessionStorage.getItem('noteData')) || initialSessionData;
  }

  // set new data in session and update container state

  setSessionData = (data) => {
    window.sessionStorage.setItem('noteData', JSON.stringify(data));
    this.setState({ data });
  }

  // set an empty note on container state for initial load
  // or when adding a new note

  initialNote = {
    author: {},
    content: {
      title: '',
      text: ''
    }
  }

  // set initial container state
  // get existing notes and set default note for the dialog

  state = {
    data: this.getSessionData(),
    isDialogActive: false,
    activeNote: this.initialNote
  };

  // add, edit, delete state data

  // use existing notes to populate random data like author and photo
  // add new note to front of collection and save to session

  addNote = () => {
    const newNote = this.generateRandomContent();
    const newData = [newNote].concat(this.state.data);
    this.setSessionData(newData);
  }

  // edit existing note and save to session
  
  editNote = () => {
    const activeNote = this.state.activeNote;
    const newData = this.state.data.map(note => {
      return note.id === activeNote.id ? activeNote : note;
    });
    this.setSessionData(newData);
  }
  
  // delete note from collection and save to session
  // (a confirm modal would be a nice TODO)

  deleteNote = (noteId) => {
    const newData = this.state.data.filter(note => (note.id !== noteId));
    this.setSessionData(newData);
  }

  // the new note dialog only captures title and description, 
  // so this helps populate random data necessary to save the note

  generateRandomContent = () => {
    const randomNote = this.state.data[Math.floor(Math.random() * this.state.data.length)];
    const newNote = {
      id: this.state.data.length + 1,
      date: new Date().toLocaleString().split(',')[0],
      author: randomNote.author,
      content: {
        ...this.state.activeNote.content,
        photo: randomNote.content.photo
      }
    };

    return newNote;
  };

  // toggle open/close the dialog and update container state

  toggleDialog = () => {
    this.setState({ 
      isDialogActive: !this.state.isDialogActive, 
    });
  }

  // update container state with the active note (edited or new) before opening dialog

  setActiveNote = (activeNote) => {
    this.setState({ activeNote: activeNote || this.initialNote });
    this.toggleDialog();
  }

  // determine whether to save edited note or save new note then close dialog

  saveDialog = () => {
    const activeNote = this.state.activeNote;
    if (activeNote.id) {
      this.editNote(activeNote);
    } else {
      this.addNote(activeNote);
    }
    this.toggleDialog();
  }

  // watch active note for changes to title and text fields in the dialog

  handleNoteUpdate = (name, value) => {
    this.setState({
      activeNote: {
        ...this.state.activeNote,
        content: {
          ...this.state.activeNote.content, 
          [name]: value 
        }
      }
    });
  };

  render() {
    return (
      <div>
        <Button 
          className={styles.addNoteButton}
          label='+ Add Note' 
          raised 
          primary 
          onClick={() => this.setActiveNote()}
        />

        <NoteDialog
          active={this.state.isDialogActive}
          closeDialog={this.toggleDialog}
          saveDialog={this.saveDialog}
          activeNoteContent={this.state.activeNote.content}
          handleNoteUpdate={this.handleNoteUpdate}
        />

        <div className={styles.noteContainer}>
          {this.state.data.map(note => 
            <Note 
              id={note.id} 
              date={note.date} 
              author={note.author}
              content={note.content}
              key={note.id} 
              editNote={() => this.setActiveNote(note)}
              deleteNote={() => this.deleteNote(note.id)}
            />
          )}
        </div>
      </div>
    );
  }
}

export default NoteContainer;