import React from 'react';
import { shallow, mount } from 'enzyme';
import sessionStorage from 'mock-local-storage';
import NoteContainer from '../../NoteContainer';
import Note from '../../../components/Note';
import { Button } from 'react-toolbox/lib/button';
import NoteDialog from '../../../components/NoteDialog';

let enzymeWrapper;
const toggleDialog = () => {
    this.setState({ 
      isDialogActive: !this.state.isDialogActive, 
    });
  }
const props = {};
const expectedNote = {
    "id": 4, 
    "date": "7/10/17",
    "author": {
        "name": "Ralph",
        "avatarUrl": "https://placeimg.com/80/80/nature"
    },
    "content": {
        "title": "Field Note Again",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim. Negat enim summo bono afferre incrementum diem. Cupiditates non Epicuri divisione finiebat, sed sua satietate. Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia.",
        "photo": "https://placeimg.com/800/450/nature/sepia"
    }
}

beforeEach(() => {
    enzymeWrapper = mount(<NoteContainer {...props} />);
});

describe('Note container', () => {
    it('should render child components', () => {
        expect(enzymeWrapper.find(NoteDialog)).toHaveLength(1);
        expect(enzymeWrapper.find(Note)).toHaveLength(5);
    });

    it('should have correct props', () => {
        const thirdNoteProps = enzymeWrapper.find(Note).get(3).props;
        expect(expectedNote.content.title).toEqual(thirdNoteProps.content.title);
        expect(expectedNote.id).toEqual(thirdNoteProps.id);
    });

    it('should open the dialog when Add Note is clicked', () => {
        expect(enzymeWrapper.instance().state.isDialogActive).toBeFalsy();
        enzymeWrapper.find(Button).first().simulate('click');
        expect(enzymeWrapper.instance().state.isDialogActive).toBeTruthy();
    });

    it('should set active note on state when Edit Note is clicked', () => {
        const firstNote = enzymeWrapper.find(Note).first();
        const editNoteButton = firstNote.find(Button).first();
        editNoteButton.simulate('click');
        const expectedTitle = enzymeWrapper.instance().state.activeNote.content.title;
        expect(expectedTitle).toBe(firstNote.props().content.title);
    });
});